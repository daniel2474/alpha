package com.tutorial.crud.service;

import com.tutorial.crud.entity.RHEmpleado;
import com.tutorial.crud.entity.RHSolicitud;
import com.tutorial.crud.repository.RHSolicitudRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RHSolicitudService {

    @Autowired
    RHSolicitudRepository rhsolicitudRepository;

    public List<RHSolicitud> list(){
        return rhsolicitudRepository.findAll();
    }

    public Optional<RHSolicitud> getOne(int id){
        return rhsolicitudRepository.findById(id);
    }

    public RHSolicitud  save(RHSolicitud solicitud){
    	return rhsolicitudRepository.save(solicitud);
    }

	public List<RHSolicitud> getByEmpleado(RHEmpleado empleado) {
    	return rhsolicitudRepository.findByEmpleado(empleado);
	}

}
